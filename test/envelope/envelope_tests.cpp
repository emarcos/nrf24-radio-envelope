#include <unity.h>
#include <RadioEnvelope.hpp>

void check()
{
    TEST_ASSERT_TRUE(true);
}

void envelopeShouldHaveFixedSize()
{
    RadioEnvelope envelope = RadioEnvelope(1, 2, T_TRIPPED);
    TEST_ASSERT_EQUAL_UINT8(8, sizeof(envelope));
}

void ctorTest()
{
    RadioEnvelope envelope = RadioEnvelope(1, 2, T_TRIPPED);
    TEST_ASSERT_EQUAL_UINT8(1, envelope.getNodeId());
    TEST_ASSERT_EQUAL_UINT8(2, envelope.getChildNodeId());
    TEST_ASSERT_EQUAL_UINT8(T_TRIPPED, envelope.getEnvelopeType());
}

void setValueWhenValueIsUint32t()
{
    RadioEnvelope envelope = RadioEnvelope(1, 2, T_TEMPERATURE);
    uint32_t value = 4294967295UL;
    envelope.setValue(value);
    TEST_ASSERT_EQUAL_UINT32(value, envelope.getUnsignedLongValue());
    TEST_ASSERT_EQUAL_UINT8(P_UNSIGNED_LONG, envelope.getPayloadType());
    TEST_ASSERT_EQUAL_STRING("4294967295", envelope.getValueAsString().c_str());
}

void setValueWhenValueIsUint8t()
{
    RadioEnvelope envelope = RadioEnvelope(1, 2, T_TEMPERATURE);
    uint8_t value = 12U;
    envelope.setValue(value);
    TEST_ASSERT_EQUAL_UINT8(value, envelope.getUnsignedByteValue());
    TEST_ASSERT_EQUAL_UINT8(P_UNSIGNED_BYTE, envelope.getPayloadType());
    TEST_ASSERT_EQUAL_STRING("12", envelope.getValueAsString().c_str());
}

void setValueWhenValueIsFloat()
{
    RadioEnvelope envelope = RadioEnvelope(1, 2, T_TEMPERATURE);
    float value = 3.19F;
    envelope.setValue(value);
    TEST_ASSERT_EQUAL_FLOAT(value, envelope.getFloatValue());
    TEST_ASSERT_EQUAL_UINT8(P_FLOAT, envelope.getPayloadType());
    TEST_ASSERT_EQUAL_STRING("3.19", envelope.getValueAsString().c_str());
}

void setValueWhenValueIsBool()
{
    RadioEnvelope envelope = RadioEnvelope(1, 2, T_TEMPERATURE);
    envelope.setValue(true);
    TEST_ASSERT_TRUE(envelope.getBooleanValue());
    TEST_ASSERT_EQUAL_UINT8(P_BOOLEAN, envelope.getPayloadType());
    TEST_ASSERT_EQUAL_STRING("true", envelope.getValueAsString().c_str());

    envelope.setValue(false);
    TEST_ASSERT_FALSE(envelope.getBooleanValue());
    TEST_ASSERT_EQUAL_UINT8(P_BOOLEAN, envelope.getPayloadType());
    TEST_ASSERT_EQUAL_STRING("false", envelope.getValueAsString().c_str());
}

void process()
{
    UNITY_BEGIN();
    RUN_TEST(check);
    RUN_TEST(ctorTest);
    RUN_TEST(envelopeShouldHaveFixedSize);
    RUN_TEST(setValueWhenValueIsUint32t);
    RUN_TEST(setValueWhenValueIsUint8t);
    RUN_TEST(setValueWhenValueIsFloat);
    RUN_TEST(setValueWhenValueIsBool);
    UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void setup()
{
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    process();
}

void loop()
{
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(500);
}

#else

int main(int argc, char **argv)
{
    process();
    return 0;
}

#endif