#include <unity.h>
#include <RadioEnvelope.hpp>

void Check()
{
    TEST_ASSERT_TRUE(true);
}

void envelopeTypeEnumTests()
{
    TEST_ASSERT_EQUAL_UINT8(0, T_PING);
    TEST_ASSERT_EQUAL_UINT8(1, T_TEMPERATURE);
    TEST_ASSERT_EQUAL_UINT8(2, T_HUMIDITY);
    TEST_ASSERT_EQUAL_UINT8(3, T_TRIPPED);
}

void commandTypeEnumTests()
{
    TEST_ASSERT_EQUAL_UINT8(0, C_PRESENTATION);
    TEST_ASSERT_EQUAL_UINT8(1, C_SET);
    TEST_ASSERT_EQUAL_UINT8(2, C_INTERNAL);
}

void payloadTypeEnumTests()
{
    TEST_ASSERT_EQUAL_UINT8(0, P_UNSIGNED_LONG);
    TEST_ASSERT_EQUAL_UINT8(1, P_UNSIGNED_BYTE);
    TEST_ASSERT_EQUAL_UINT8(2, P_BOOLEAN);
    TEST_ASSERT_EQUAL_UINT8(3, P_FLOAT);
}

void process()
{
    UNITY_BEGIN();
    RUN_TEST(Check);
    RUN_TEST(envelopeTypeEnumTests);
    RUN_TEST(commandTypeEnumTests);
    RUN_TEST(payloadTypeEnumTests);
    UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>
void setup()
{
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    delay(2000);

    process();
}

void loop()
{
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(500);
}

#else

int main(int argc, char **argv)
{
    process();
    return 0;
}

#endif