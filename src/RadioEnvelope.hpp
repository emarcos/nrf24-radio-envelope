#pragma once

#include <Arduino.h>
#include <stdint.h>

typedef enum : uint8_t
{
    T_PING = 0,
    T_TEMPERATURE,
    T_HUMIDITY,
    T_TRIPPED
} EnvelopeTypeEnum;

typedef enum : uint8_t
{
    C_PRESENTATION = 0,
    C_SET,
    C_INTERNAL
} CommandTypeEnum;

typedef enum : uint8_t
{
    P_UNSIGNED_LONG = 0,
    P_UNSIGNED_BYTE,
    P_BOOLEAN,
    P_FLOAT
} PayloadTypeEnum;

class RadioEnvelope
{
public:
    RadioEnvelope() {}

    RadioEnvelope(uint8_t nodeId, uint8_t childNodeId, EnvelopeTypeEnum envelopeType)
    {
        this->nodeId = nodeId;
        this->childNodeId = childNodeId;
        this->envelopeType = envelopeType;
    }

    uint8_t getNodeId()
    {
        return this->nodeId;
    }

    uint8_t getChildNodeId()
    {
        return this->childNodeId;
    }

    EnvelopeTypeEnum getEnvelopeType()
    {
        return (EnvelopeTypeEnum)this->envelopeType;
    }

    RadioEnvelope &setValue(const uint32_t value)
    {
        this->payloadType = P_UNSIGNED_LONG;
        this->unsignedLongValue = value;
        return *this;
    }

    RadioEnvelope &setValue(const uint8_t value)
    {
        this->payloadType = P_UNSIGNED_BYTE;
        this->unsignedByteValue = value;
        return *this;
    }

    RadioEnvelope &setValue(const float value)
    {
        this->payloadType = P_FLOAT;
        this->floatValue = value;
        return *this;
    }

    RadioEnvelope &setValue(const bool value)
    {
        this->payloadType = P_BOOLEAN;
        this->unsignedByteValue = value;
        return *this;
    }

    bool getBooleanValue() const
    {
        return this->unsignedByteValue;
    }

    uint32_t getUnsignedLongValue() const
    {
        return this->unsignedLongValue;
    }

    uint32_t getUnsignedByteValue() const
    {
        return this->unsignedByteValue;
    }

    float getFloatValue() const
    {
        return this->floatValue;
    }

    PayloadTypeEnum getPayloadType() const
    {
        return (PayloadTypeEnum)this->payloadType;
    }

    String getValueAsString() const
    {
        switch (this->payloadType)
        {
        case P_UNSIGNED_LONG:
            return String(this->unsignedLongValue);
            break;
        case P_UNSIGNED_BYTE:
            return String(this->unsignedByteValue);
            break;
        case P_FLOAT:
            return String(this->floatValue);
            break;
        case P_BOOLEAN:
            if (unsignedByteValue == 1)
            {
                return String("true");
            }
            else
            {
                return String("false");
            }
            break;
        default:
            return String("unknown");
            break;
        }
    }

private:
    union
    {
        struct
        {
            uint8_t nodeId;
            uint8_t childNodeId;
            PayloadTypeEnum payloadType;
            EnvelopeTypeEnum envelopeType;
            union
            {
                uint32_t unsignedLongValue;
                float floatValue;
                uint8_t unsignedByteValue;

            } __attribute__((packed));
        };
    };
};
